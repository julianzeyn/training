/** 
 * Annotations start with "@".
 * Annotations provide info and data about the code, but does not influence it.
 * Can be used to help the compiler detect errors or suppress warnings.
 * Tests use the @Test annotation.
 * Frameworks can use annotations in different ways.
 * New annotations can be defined by the programmer.
 *
 * Some annotation types:
 * @Deprecated -> marked element should not be used
 *                compiler generates a warning (should also be documented in the Javadoc)
 * @Override -> marked element is meant to override an element from the superclass
 *              compiler generates an error when not overriding
 * @SuppressWarnings -> suppress specific warnings when compiling
 *                      takes an argument
 * @FunctionalInterface -> type declaration is intended to be a functional interface (only one abstract method)
 * @Documented -> indicates that an annotation should be documented in the Javadoc code
 */
 
class Annotations {
    public static void main(String[] args) {
        System.out.println("Read the Annotations.java file.");
    }
}