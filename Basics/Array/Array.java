class Array {
	public static void main(String[] args){
		int[] arr = new int[5];
		arr[0] = 21;
		arr[1] = 12;
		arr[2] = 42;
		arr[3] = 56;
		arr[4] = 10;
		String[] arr2 = {"eins", "zwei", "drei"};
		for (int i: arr) {
			System.out.println(i);
		}
		for (int i = 0; i < arr2.length; i++) {
			System.out.println(arr2[i]);
		}
		System.out.println("3-dimensional array:");
		int[][][] arr3 = {{{111, 112}, {121, 122}}, {{211, 212}, {221, 222}}};
		//System.out.println(arr2); //this does not work
		//System.out.println(arr3.length); // length == 2, first dimension
		for (int i = 0; i < arr3.length; i++) {
			for (int j = 0; j < arr3[0].length; j++) {
				for (int k = 0; k < arr3[0][0].length; k++) {
					System.out.println(arr3[i][j][k]);
				}
			}
		}
		int[][][] arr4 = new int[2][4][6];
		System.out.println(arr4.length); // 1st dimension
		System.out.println(arr4[0].length); // 2nd dimension
		System.out.println(arr4[1].length); // 2nd dimension, but could be different 
											//("jagged" arrays, different number of components in each "row")
		System.out.println(arr4[0][0].length); // 3rd dimension
	}
}