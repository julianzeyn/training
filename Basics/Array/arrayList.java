import java.util.ArrayList;
// import java.util.LinkedList; // this is similar to ArrayList
// ArrayList -> better to access the stored objects
// LinkedList -> better to change the stored data
import java.util.Collections;
// needed to sort a List
import java.util.Iterator;
// iterator, that contains one element of a list and circulates through this list
// used when deleting elements from the arrayList
// deleting elements in a for-loop can result in a IndexOutofBoundsException

class arrayList {
	public static void main (String[] args) {
		// ArrayList colours = new ArrayList(); 
		// unsafe, works, but gives warnings when compiling
		ArrayList<String> colours = new ArrayList<String>();
		System.out.println("Size of Array: " + colours.size());
		colours.add("red");
		colours.add("blue");
		colours.add("green");
		colours.add("yellow");
		System.out.println("Size of Array: " + colours.size());
		for (int i = 0; i < colours.size(); i++) {
			System.out.println(colours.get(i));
		}
		colours.remove("green");
		System.out.println(colours);
		Collections.sort(colours);
		System.out.println(colours);
		Iterator<String> iter = colours.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
		if (!colours.contains("green")){
			colours.clear();
			System.out.println("content of the ArrayList deleted, because it didn't contain 'green'");
		}
		System.out.println("Size of Array: " + colours.size());
	}
}