import java.util.HashMap;
// a "HashMap" is a list that stores pairs of "key" and "value"
// the keys are unique
// when creating another pair with the same key, the first one gets overwritten

class hashMap {
	public static void main(String[] args) {
		HashMap<String, Integer> hash1 = new HashMap<>();
		hash1.put("Julian", 12);
		hash1.put("Ying", 15);
		System.out.println("Julian: " + hash1.get("Julian"));
		System.out.println("Ying: " + hash1.get("Ying"));
		hash1.remove("Ying");
		hash1.put("Ying", 12);
		hash1.put("Julian", 15);
		if (hash1.containsKey("Ying")) {
			System.out.println("Ying has " + hash1.get("Ying") + " points.");
		}
		if (hash1.containsValue(15)) {
			System.out.println("Someone has 15 points.");
		}
		HashMap<Integer, String> hash2 = new HashMap<>();
		hash2.put(1, "Julian");
		hash2.put(1, "Ying");
		hash2.put(2, "Julian");
		hash2.put(3, "Julian");
		System.out.println("1: " + hash2.get(1));
		System.out.println("2: " + hash2.get(2));
		System.out.println("3: " + hash2.get(3));
	}
}