import java.util.HashSet;
// list of unique values
// order not consistent
// to fix it: "LinkedHashSet"

class hashSet {
	public static void main(String[] args) {
		HashSet<String> colours = new HashSet<>();
		colours.add("red");
		colours.add("green");
		colours.add("blue");
		colours.add("yellow");
		System.out.println(colours);
		colours.add("orange");
		colours.add("green");
		System.out.println(colours);
	}
}