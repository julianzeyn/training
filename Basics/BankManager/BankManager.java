class BankAccount {
	private String accountHolder;
	private int accountNumber;
	private double accountBalance;
	
	protected BankAccount (String accountHolder, int accountNumber, double accountBalance) {
		this.accountHolder = accountHolder;
		this.accountNumber = accountNumber;
		this.accountBalance = accountBalance;
	}
	protected BankAccount (String accountHolder, int accountNumber) {
		this(accountHolder, accountNumber, 0.);
	}
	
	protected String getAccountHolder() {
		return accountHolder;
	}
	protected int getAccountNumber() {
		return accountNumber;
	}
	protected double getBalance() {
		return accountBalance;
	}
	protected void setBalance(double amount) {
		accountBalance = amount;
		System.out.println("The balance of " + accountHolder + "s account is: " + amount);
	}
	protected void deductFromAccount(double withdrawal) {
        accountBalance -= withdrawal;
		//accountBalance = accountBalance - withdrawal;
		System.out.println(withdrawal + " have been deducted from " + accountHolder + "s account.");
	}
	protected void addToAccount(double deposit) {
        accountBalance += deposit;
		//accountBalance = accountBalance + deposit;
		System.out.println(deposit + " have been deposited into " + accountHolder + "s account.");
    }
}
 
class SavingsAccount extends BankAccount {
	private int interestRate;
    
    protected SavingsAccount(String accountHolder, int accountNumber, double accountBalance, int interestRate) {
		super(accountHolder, accountNumber, accountBalance);
        this.interestRate = interestRate;
	}
    protected SavingsAccount(String accountHolder, int accountNumber) {
		this(accountHolder, accountNumber, 0., 5);
	}
	
    protected void setInterestRate(int interestRate) {
        this.interestRate = interestRate;
    }
    protected int getInterestRate() {
        return interestRate;
    }
    protected double calculateInterestEarned(){
        double interestEarned = getBalance() * interestRate / 100;
        addToAccount(interestEarned);
        return interestEarned;
    }
}

class BankManager {
	
	public static void main(String[] args) {
		BankAccount juliansAccount = new BankAccount("Julian", 1);
		BankAccount yingsAccount = new BankAccount("Ying", 2, 1000.5);
		
		System.out.println("Julian has: " + juliansAccount.getBalance());
		juliansAccount.setBalance(150.75);
		System.out.println("Julian has: " + juliansAccount.getBalance());
        juliansAccount.addToAccount(34.54);
        System.out.println("Julian has: " + juliansAccount.getBalance());
        juliansAccount.deductFromAccount(78.54);
        System.out.println("Julian has: " + juliansAccount.getBalance());
		
		System.out.println("Yings Account belongs to " + yingsAccount.getAccountHolder() + ", has the account number \"" + yingsAccount.getAccountNumber() + "\" and contains " + 	yingsAccount.getBalance());
        
        SavingsAccount someonesAccount = new SavingsAccount("Someone", 3, 100, 10);
        SavingsAccount someoneElsesAccount = new SavingsAccount("Someone Else", 4);
        System.out.println("Someone has: " + someonesAccount.getBalance());
        System.out.println("Someone earned: " + someonesAccount.calculateInterestEarned());
        System.out.println("Someone has: " + someonesAccount.getBalance());
        someoneElsesAccount.setBalance(1000);
        System.out.println("Someone Elses interest rate: " + someoneElsesAccount.getInterestRate());
        someoneElsesAccount.setInterestRate(3);
        System.out.println("Someone Elses interest rate: " + someoneElsesAccount.getInterestRate());
        System.out.println("Someone Else has: " + someoneElsesAccount.getBalance());
        System.out.println("Someone Else earned: " + someoneElsesAccount.calculateInterestEarned());
        System.out.println("Someone Else has: " + someoneElsesAccount.getBalance());
	}
}