/** 
 * Overview about Sets, Lists, Queues, Maps.
 *
 * Set -> cannot contain duplicate elements
 * SortedSet -> Set with elements in ascending order
 * List -> ordered, can contain duplicate elements, elements can be accessed by their index
 * Queue -> holds multiple elements prior to procession, provides additional insertion, extraction and inspection options
 *          typically order elements as FIFO (First In, First Out) can be changed, example: Priority Queue
 *          (ordered by a comparator or natural order), only the top element can be inspected/extracted/removed
 * Deque -> "Double Ended Queue", can be used as FIFO or LIFO (Last In, First Out), replaced Stacks,
 *          elements can be inserted and extracted from both ends
 * Map -> maps keys to values, keys are unique, keys can only map to one value
 * SortedMap -> Map with ascending key order
 *
 * all Collections:
 * size(), isEmpty(), contains(), add(), remove(), iterator(), stream(), parallelStream()
 * containsAll(), addAll(), removeAll(), clear(), retainAll()
 *
 * Arrays.asList()
 * toArray(new <Type>[size])
 */

import java.util.*;

class CollectionTypes {
    public static void main (String[] args) {
        Collection<Integer> hs = new HashSet<>(); // order is not fixed
        Set<String> ts = new TreeSet<>(); // (natural) ordered Set
        LinkedHashSet<Character> lhs = new LinkedHashSet<>(); // insertion ordered Set
        // add remove
        
        Collection<Integer> al = new ArrayList<>();
        List<String> ll = new LinkedList<>();
        // add get set remove
        // Collections.sort shuffle reverse rotate swap replaceAll fill copy
        
        Collection<Integer> pq = new PriorityQueue<>();
        Queue<String> q = new LinkedList<>();
        // add poll peek
        
        Collection<Integer> ad = new ArrayDeque<>();
        Deque<String> d = new LinkedList<>();
        // addFirst addLast pollFirst pollLast peekFirst peekLast
        // removeFirstOccurence removeLastOccurence
        
        Map<Integer, Integer> hm = new HashMap<>(); // order is not fixed
        Map<String, String> tm = new TreeMap<>(); // (natural) ordered Map
        LinkedHashMap<Character, Integer> lhm = new LinkedHashMap<>(); // insertion ordered Map
        // put, get, remove, containsKey, containsValue, size, isEmpty, putAll, clear, keySet, entrySet, values
        
        System.out.println("Read the CollectionTypes.java file");
    }
}