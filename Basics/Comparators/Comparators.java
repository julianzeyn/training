/** Learning about making a custom Comparator. */

import java.util.Arrays;
import java.util.Comparator;

class SortingThis {
    int firstVar; // can be directly accessed
    private String secondVar; // needs a getter method
    
    SortingThis (int firstVar, String secondVar) {
        this.firstVar = firstVar;
        this.secondVar = secondVar;
    }
    
    String getSecondVar() {
        return secondVar;
    }
}

class Comparators {
    static class StringComparator implements Comparator<SortingThis> { // a comparator can be defined as a class
        @Override
        public int compare(SortingThis firstObject, SortingThis secondObject) { // @Override the compare method of the interface
            int Comparison = firstObject.getSecondVar().compareTo(secondObject.getSecondVar()); 
            return Comparison;
            // compare method returns:
            // negative integer for "first argument comes first"
            // 0 for "they're equally ordered"
            // positive integer for the "first argument comes second"
            // swap order of objects or multiply result by (-1) to get reverse ordering
        }
    }
    
    public static void main(String[] args) {
        SortingThis[] testArray = new SortingThis[5];
        String testString;
        for (int i = 0; i < 5; i++) {
            testString = "test" + i;
            testArray[i] = new SortingThis(5 - i, testString);
        }
        System.out.println("After creation: ");
        for (SortingThis el : testArray) {
            System.out.println(el.firstVar + " " + el.getSecondVar());
        }
        Arrays.sort(testArray, new Comparator<SortingThis>() { // a comparator can also be defined as a anonymous class
            @Override
            public int compare(SortingThis firstObject, SortingThis secondObject) { // @Override the compare method of the interface
                return firstObject.firstVar-secondObject.firstVar;
                // compare method returns:
                // negative integer for "first argument comes first"
                // 0 for "they're equally ordered"
                // positive integer for the "first argument comes second"
                // swap order of objects or multiply result by (-1) to get reverse ordering
            };
        });
        System.out.println("After sorting for the Integer: ");
        for (SortingThis el : testArray) {
            System.out.println(el.firstVar + " " + el.getSecondVar());
        }
        Arrays.sort(testArray, new StringComparator());
        System.out.println("After sorting for the String: ");
        for (SortingThis el : testArray) {
            System.out.println(el.firstVar + " " + el.getSecondVar());
        }
        // using 3 variations of Lambda expressions (all 3 give the same result)
        Arrays.sort(testArray, (firstObject, secondObject) -> {return firstObject.firstVar - secondObject.firstVar;});
        Arrays.sort(testArray, (SortingThis firstObject, SortingThis secondObject) -> firstObject.firstVar - secondObject.firstVar);
        Arrays.sort(testArray, (firstObject, secondObject) -> firstObject.firstVar - secondObject.firstVar);
        System.out.println("After sorting for the Integer with Lambda expressions: ");
        for (SortingThis el : testArray) {
            System.out.println(el.firstVar + " " + el.getSecondVar());
        }
    }
}