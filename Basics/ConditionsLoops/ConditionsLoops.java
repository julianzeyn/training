class ConditionsLoops {
	public static void main (String[] args) {
		System.out.println("Various tests of conditionals and loops:");
		int x = 7;
		if (x < 10) {
			System.out.println("x is less than 10.");
		} else if (x > 20) {
			System.out.println("x is greater than 20.");
		} else {
			System.out.println("x is between 10 and 20.");
		}
		
        String[] array = {"Ron", "Harry", "Hermoine"};
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
        System.out.print("\n");
        // shorter code:
        for (String i : array) {
            System.out.print(i + " ");
        }
		System.out.print("\n");
        
		while (x > 0) {
			System.out.println("x is: " + x);
			x--;
		}
		
		do {
			System.out.println("x is: " + x);
			x++;
		} while (x < 5);
		
		int day = 6;
		switch (day) {
			case 1:
			System.out.println("Today is Monday.");
			break;
			case 2:
			System.out.println("Today is Tuesday.");
			break;
			case 3:
			System.out.println("Today is Wednesday.");
			break;
			case 4:
			System.out.println("Today is Thursday.");
			break;
			case 5:
			System.out.println("Today is Firday.");
			break;
			case 6:
			System.out.println("Today is Saturday.");
			// break; 
            // it will fall through to the next case
			case 7:
			System.out.println("Today is Sunday.");
			break;
			default:
			System.out.println("Error. Not a weekday. Use 1 <= day <= 7.");
		}
		
	}
}
	