class Throw {
	static int div(int a, int b) throws ArithmeticException {
		if (b == 0) {
			throw new ArithmeticException("Division by Zero!");
		} else {
			return a/b;
		}
	}
	// this still needs a try/catch-block in the main method

	public static void main(String[] args) {
		System.out.println(div(4, 0));
		System.out.println("This will not be executed when divided by zero.");
	}
}