public class TryCatch {
    public static void main(String[] args) {
        int[] arr = new int[10];
        try {
            System.out.println(arr[9001]);
        } catch (Exception ex) {
            System.out.println("Problem with code detected");
        }
		// "try" to run the code
		// "catch" runs when encountering an error ("Exception" / "ArrayIndexOutOfBoundsException")
    }
}