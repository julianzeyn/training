import java.io.File;
// creating and working with files
import java.util.Scanner;
import java.util.ArrayList;

class Files {
	public static void main(String[] args) {
		String fileName = "C:\\Users\\Admin\\Desktop\\Programming\\Java\\projects\\Files\\Files.txt";
		File file = new File(fileName);
		if (file.exists()) {
			ArrayList<String> list = new ArrayList<>();
			System.out.println(file.getName());
			try { // try-catch or throw for error handling, otherwise error during compiling
                Scanner scan = new Scanner(file).useDelimiter("\\s*\n\\s*");
                // useDelimiter defines where to split the "scan.next()"-method
                while (scan.hasNext()) {
                    list.add(scan.next());
                }
                scan.close();
            } catch (Exception e) {
                System.out.println("Error!");
            }
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
		}
	}
}