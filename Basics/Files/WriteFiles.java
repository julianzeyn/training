import java.util.Formatter; // writing to a file

class WriteFiles {
    public static void main(String[] args) {
        String fileName = "C:\\Users\\Admin\\Desktop\\Programming\\Java\\projects\\Files\\Files.txt";
        try { // try-catch or throw for error handling, otherwise error during compiling
            Formatter f = new Formatter(fileName); // opens/creates a file (gets overwritten!)
            f.format("%s %s %s", "1", "Julian", "Zeyn \r\n"); // "%s" are placeholders
            f.format("%s %s %s", "2", "Ying", "Chu \r\n"); // they get replaced by the strings (in order)
            f.format("%s %s %s", "3", "Name", "Lastname \r\n"); // "\r\n" for Windows, "\n" for Linux
            f.close();
        } catch (Exception e) {
            System.out.println("Error!");
        }
    }
}