class Student {
    private String firstName;
    private String lastName;
    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public void printFullName() {
        System.out.println(this.firstName + " " + this.lastName);
    }
	// no "static" -> is called with ""className".printFullName" and not with "Student.printFullName"
}

public class Functions {
    public static void main(String[] args) {
        Student[] students = new Student[] {
            new Student("Morgan", "Freeman"),
            new Student("Brad", "Pitt"),
            new Student("Kevin", "Spacey"),
        };
        for (Student s : students) { // for-loop that runs for each element of the array "students"
            s.printFullName();
        }
    }
}