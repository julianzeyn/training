/** 
 * Learning how to create a GUI with java.
 * 
 * Frame -> window where the components are held
 * Content pane -> a "background", on which the components are placed
 * Layout Manager -> aligns components within content pane
 * Coordinates -> used to specify where components are placed on the content pane
 *                (not using a layout manager)
 *             -> (X,Y)-values relative to the top left corner
 */

import javax.swing.*; // in this example needed for JFrame, JLabel, JTextField, JButton
import java.awt.event.*; // needed for ActionListener, ActionEvent
import java.awt.*; // needed for Container, GridLayout

class RectangleAreaCalculator extends JFrame { // need a class to extend JFrame
    private static final int HEIGHT = 400;
    private static final int WIDTH = 700;
    
    private JLabel lengthLabel;
    private JLabel widthLabel;
    private JLabel areaLabel;

    private JTextField lengthText;
    private JTextField widthText;
    private JTextField areaText;

    private JButton calculateButton;
    private JButton exitButton;
    
    private CalculateButtonHandler calculateButtonHandler;
    private ExitButtonHandler exitButtonHandler;
    
    RectangleAreaCalculator() { // Constructor
        lengthLabel = new JLabel("Please enter the rectangle's length: ", SwingConstants.RIGHT); // RIGHT aligned text
        widthLabel = new JLabel("Please enter the rectangle's width: ", SwingConstants.RIGHT);
        areaLabel = new JLabel("The Area is: ", SwingConstants.RIGHT);
        
        lengthText = new JTextField(12);
        widthText = new JTextField(12);
        areaText = new JTextField(12);
        
        calculateButton = new JButton("What is the Area?");
        calculateButtonHandler = new CalculateButtonHandler();
        calculateButton.addActionListener(calculateButtonHandler);
        exitButton = new JButton("Close");
        exitButtonHandler = new ExitButtonHandler();
        exitButton.addActionListener(exitButtonHandler);
        
        setTitle("Rectangle Area Calculator");
        Container pane = getContentPane();
        pane.setLayout(new GridLayout (4, 2)); // Layout Manager places the components in a grid layout
        pane.add(lengthLabel);
        pane.add(lengthText);
        pane.add(widthLabel);
        pane.add(widthText);
        pane.add(areaLabel);
        pane.add(areaText);
        pane.add(calculateButton);
        pane.add(exitButton);
        
        setSize(WIDTH, HEIGHT);
        setVisible(true); // important, otherwise invisible window
        setDefaultCloseOperation(EXIT_ON_CLOSE);   
    }
    
    private class CalculateButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            double width;
            double length;
            double area;
            length = Double.parseDouble(lengthText.getText());
            width = Double.parseDouble(widthText.getText());
            area = length * width;
            areaText.setText("" + area);
        }
    }

    private class ExitButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}
    
class GUI {
    public static void main(String[] args) {
        RectangleAreaCalculator rectObj = new RectangleAreaCalculator();
    }
}