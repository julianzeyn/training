import java.util.*;
// needed to use List and ArrayList: "List<Double> r=new ArrayList<>();"
public class Generics{
	// Generics are in <>-brackets
	// Example: List<String> = ...
	// prevents the inserting of other types into the list
    public static class FavoriteClasses<Class1, Class2, Class3> {
		// Generics can also be used to define classes
		// enables multiple options for classes of variables
		// Class1/2/3 are the placeholders for the real types
        private Class1 favorite1;
        private Class2 favorite2;
        private Class3 favorite3;
        FavoriteClasses(Class1 fav1, Class2 fav2, Class3 fav3) {
            this.favorite1 = fav1;
            this.favorite2 = fav2;
            this.favorite3 = fav3;
        }
        public Class1 getFav1() {
            return(this.favorite1);
        }
        public Class2 getFav2() {
            return(this.favorite2);
        }
        public Class3 getFav3() {
            return(this.favorite3);
        }
    }
    public static void main(String[] args) {
        List<Double> r=new ArrayList<>();
        r.add(6.3);
        r.add(5.9);
        FavoriteClasses<String, Integer, Double> a = new FavoriteClasses<>("Hello",67,r.get(0));
		// when creating an object with generic types, they have to be declared
		// you cannot use primitives (int, double, long, char, ...)
		// you have to use their class versions (Integer, Double, Long, Char, ...)
        System.out.println("My favorites are " + a.getFav1() + ", " + a.getFav2() + ", and " + a.getFav3() + ".");
    }
}
