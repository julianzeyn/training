class myGenericClass<Gen1, Gen2> { // generic class with the generic types Gen1 and Gen2
    Gen1 variable1;  
    Gen2 variable2;
    public myGenericClass(Gen1 variable1, Gen2 variable2) {
        this.variable1 = variable1;
        this.variable2 = variable2;
    }
    public Gen1 getVariable1() {
        return variable1;
    }
    public Gen2 getVariable2() {
        return variable2;
    }
}

class Generics2 {
    public static void main(String[] args) {
        myGenericClass<Integer, String> intstrvar = new myGenericClass<>(30,"I love Generic types");
		// the types do not have to be repeated in the second pair of <>-brackets 
        System.out.println(intstrvar.getVariable1());
        System.out.println(intstrvar.getVariable2());

    }
}