// Comments
// Code to run has to be inside of a "class"
// create class "HelloWorld"
class HelloWorld {
	//IMPORTANT: name of the class containing the "main"-method should match the file name: HelloWorld.java
	//often used: Main

	//main method:
	public static void main(String[] args) {
		//public -> other classes have access
		//static -> method can be run without creating an instance of it
		//void -> no return value
		//main -> name of the method
		//IMPORTANT: main is the method that is run
		//args -> arguments to run (array of strings)
		//EXAMPLE: "java HelloWorld arg1 arg2 arg3
		System.out.println("Hello, World!");
		//System -> pre-defined class
		//out -> output
		//println -> pre-defined method that returns a line of text
	}
}