public class Main {
	// public -> every class has access
	// default -> every class in the same package has access
	// protected -> like "default", but subclasses have access to the superclass' methods and variables
	// private -> only the class itself has acces to it
	
  public static void main(String[] args) {
	  // void -> no return value
	  // static -> does not belong to an instance (exists only once)
    System.out.println("Hello World");
  }
}

