public class Tutorial {
	
	public static void main(String[] args) {
		
		System.out.println("Hello world!");
		String name = "Julian";
		System.out.println("Hello " + name + "!\r\n");
		
		if (checkUser(name)) {
			grantPermission();
		}
		else {
			denyPermission();
		}
	}
	
	static void grantPermission() {
		System.out.println("Special user privileges granted!");
	}
	
	static void denyPermission() {
		System.out.println("No special privileges granted.");
	}
	
	static boolean checkUser(String nameCheck) {
		if (nameCheck == "Julian") {
			return true;
		}
		else {
			return false;
		}
	}
}