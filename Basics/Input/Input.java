// for Scanner class:
import java.util.*; 

// for BufferedReader:
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


class Input {
	public static void main(String[] args) throws IOException { // BufferedReader needs IOException handling
		String input = "CommandLineOptions"; // "Scanner", "BufferedReader", "ConsoleClass", "CommandLineOptions"
        switch (input) {
            case "Scanner":
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please enter a sentence:");
            System.out.println("You entered: " + scanner.nextLine());
            System.out.println("Please enter an integer:");
            System.out.println("You entered: " + scanner.nextInt());
            scanner.close(); // close the scanner at the end
            // a new scanner can not be created
            break;
            
            case "BufferedReader":
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); // code to create a BufferedReader
            System.out.println("Type something!");
            String name = reader.readLine();
            System.out.println("You typed " + name);
            // needs parsing to get other types from the String
            break;
            
            case "ConsoleClass":
            System.out.print("Type something: ");
            String name2 = System.console().readLine();
            System.out.println("You typed " + name2);
            break;
            
            case "CommandLineOptions":
            if (args.length > 0) {
                System.out.println("The command line arguments are:");
                for (String val : args) {
                    System.out.println(val);
                }
            } else {
                System.out.println("No command line arguments found.");
            }
        }
	}
}