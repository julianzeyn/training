/**
 * This is a Javadoc comment to automatically generate documentation, it is written in HTML.
 * A Javadoc comment block is typically placed at the beginning of the file and before each class, constructor and method.
 * You get the documentation by running "javadoc filename.java".
 * <p>
 * The line above this is code for HTML and creates a paragraph break.
 * Javadoc comments precede a class, constructor or method declaration to provide explanation.
 * At the end of a block of text, there are block tags, preceded by "@".
 * There is a blank line preceding the block tags.
 *
 * @author (multiple tags possible)
 * @version (or @since)
 * @param   param-name    explanation of the input
 * @return                explanation of the output
 * @exception (or @throws)
 * @see (or @link)        link with further information
 * @deprecated (what to use as an replacement?)
 */

class Javadoc {
    public static void main (String[] args) {
        System.out.println("Read the Javadoc.java file.");
    }
}