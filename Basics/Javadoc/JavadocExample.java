/**
 * This is an example for the Javadoc comments.
 *
 * @author Julian Zeyn
 * @version 1.0
 */

public class JavadocExample {
    /**
     * @param args unused
     */
    public static void main (String[] args) {
        /**
         * Prints "This is a test!"
         */
        System.out.println("This is a test!");
    }
}