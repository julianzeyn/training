/**
 * Lambda expressions are used to implement functional interfaces.
 * Functionsal interfaces have only one abstract method.
 * Lambda expressions are also used for streams. (see Streams.java)
 */

@FunctionalInterface // not necessary, but helps to catch errors during compiling
interface MyInterface {
    // abstract method
    void getValue();
}
@FunctionalInterface
interface My2ndInterface {
    // abstract method
    int getValue(int x);
}
// a generic interface enables the Lambda expressions to have more freedom
@FunctionalInterface
interface GenericInterface<T> {
    // generic, abstract method
    T func(T t);
}

class Lambda {
    public static void main (String[] args) {
        // Lambda expression without parameters
        MyInterface test = () -> System.out.println("This was printed by using a Lambda expression.");
        test.getValue();
        // Lambda expressions with one parameter (more are possible)
        My2ndInterface test2 = (x) -> { // brackets are optional
            int y = 2 * x;
            return y;
        };
        int result = test2.getValue(10);
        System.out.println(result);
        // Generic interface function one:
        GenericInterface<String> reverse = (str) -> {
            String output = "";
            for (int i = str.length()-1; i >= 0 ; i--)
            output += str.charAt(i);
            return output;
        };
        System.out.println("Lambda reversed = " + reverse.func("Lambda"));
        // Generic interface function two:
        GenericInterface<Integer> factorial = (n) -> {
            int output = 1;
            for (int i = 1; i <= n; i++)
            output = i * output;
            return output;
        };
        System.out.println("Factorial of 5 = " + factorial.func(5));
    }
}