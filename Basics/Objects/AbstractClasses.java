abstract class abstractClass { 
// abstract classes have one or more abstract functions/methods
    abstract void abstractMethod(); 
	// abstract funktions/methods have no code to run
	// they only create the method
	// the subclasses have to implement these abstract methods
    void concreteMethod() { // concrete methods are still allowed in abstract classes 
        System.out.println("This is a concrete method."); 
    } 
}

class B extends abstractClass { 
    void abstractMethod() { 
        System.out.println("B's implementation of abstractMethod."); 
    } 
}

class C extends abstractClass { 
    void abstractMethod() { 
        System.out.println("C's implementation of abstractMethod."); 
    } 
}

public class AbstractClasses { 
    public static void main(String[] args) { 
        B b = new B(); 
        b.abstractMethod(); 
        b.concreteMethod();
        C c = new C(); 
        c.abstractMethod(); 
        c.concreteMethod();
    } 
}