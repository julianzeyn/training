class class1 {
    public void class1Method() {
        System.out.println("This is the method of class1.");
    };
}

class class2 extends class1 { // classes can extend (inherit from) other classes
// class1 is the superclass, class 2 the subclass
    public void class2Method() {
        System.out.println("This is the method of class2.");
    };
}

interface interface0 { // interfaces can only have abstract methods
// this is not allowed:
/*     public void interface0ConcreteMethod() {
        System.out.println("This is the concrete method of interface0.");
    }; */ 
    public void interface0AbstractMethod();
}

interface interface1 {
    public void interface1AbstractMethod();
}

interface interface2 extends interface0, interface1 { // interfaces can extend (inherit from) one or more other interfaces
    public void interface2AbstractMethod();
}

interface interface3 {
    public void interface3AbstractMethod();
}

class class3 extends class2 implements interface2, interface3 { 
// a class can extend ONE (not multiple) class and implement one or more interfaces at the same time
        public void class3Method() {
        System.out.println("This is the method of class3.");
        };
        public void interface0AbstractMethod() {
            System.out.println("This is the implemented, abstract method of interface0.");
        };        
        public void interface1AbstractMethod() {
            System.out.println("This is the implemented, abstract method of interface1.");
        };
        public void interface2AbstractMethod() {
            System.out.println("This is the implemented, abstract method of interface2.");
        };
        public void interface3AbstractMethod() {
            System.out.println("This is the implemented, abstract method of interface3.");
        };
}

class Inheritance {
    public static void main(String[] args) {
        class3 exampleClass = new class3();
        exampleClass.class1Method();
        // class3 extends class2 extends class1 -> class3 has class1Method
        exampleClass.class2Method();
        exampleClass.class3Method();
        exampleClass.interface0AbstractMethod();
        exampleClass.interface1AbstractMethod();
        // class3 implements interface2 extends interface0, interface1
        // -> class3 has interface0AbstractMethod, interface1AbstractMethod
        exampleClass.interface2AbstractMethod();
        exampleClass.interface3AbstractMethod();
    }
}
    