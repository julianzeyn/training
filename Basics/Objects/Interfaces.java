interface Animal {
	void feed();
	void groom();
	void pet();
}
// an "Interface" is a template for a class
// all of its functions/methods are abstract and have to be implemented by the class that inherits it

class Cat implements Animal {
	public void feed() {
		System.out.println("The cat has been fed.");
	}
	public void groom() {
		System.out.println("The fur has been brushed.");
	}
	public void pet() {
		System.out.println("Purr.");
	}
}

class Dog implements Animal {
	public void feed() {
		System.out.println("The dog has been fed.");
	}
	public void groom() {
		System.out.println("The fur has been washed.");
	}
	public void pet() {
		System.out.println("*Happy dog noises* (I don't know what sound they make when pet.)");
	}
}

public class Interfaces {
	public static void main(String[] args) {
		Cat myCat = new Cat();
		Dog myDog = new Dog();
		myCat.feed();
		myCat.groom();
		myCat.pet();
		myDog.feed();
		myDog.groom();
		myDog.pet();
	}
}