class Point {
    private double x;
    private double y;
    // private -> can only be accessed and changed by methods in this class
	
	public Point() { // constructor (overloaded), is called when an object is created
	// the constructor has the same name as the class
	// overloaded -> more than one method with the same name
	// does different things dependeíng on the arguments
		this(0., 0.); // this -> calls the constructor with arguments
	}
    public Point(double x, double y) { // also constructor (overloaded)
        this.x = x;
        this.y = y;
    }
    
    public void print() {
        System.out.println("(" + x + "," + y + ")");
    }
    
    public void scale() {
        x = x/2;
        y = y/2;
    }
}

public class Objects {
    public static void main(String[] args) {
		Point o = new Point();
		o.print();
        Point p = new Point(32, 32);
        for (int i = 0; i < 6; i++) {
            p.scale();
            p.print();
        }
    }
}