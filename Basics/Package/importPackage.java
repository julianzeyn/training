// import package.name.Class;   // Import a single class
// import package.name.*;   // Import the whole package
// Examples:

import java.util.*; // import the whole java.util-package
import java.util.Scanner; // import only the Scanner-class of the java.util-package (here redundant)

class importPackage {
  public static void main(String[] args) {
    Scanner myObj = new Scanner(System.in);
    System.out.println("Enter username");

    String userName = myObj.nextLine();
    System.out.println("Username is: " + userName);
  }
}