// Java uses a file system:
// Example:
// root\\myPackage\\userDefinedPackage.java

package myPackage; // create a package

class messageClass {
    public void message() {
        System.out.println("This is my package!");
    }
}

class userDefinedPackage {
    public static void main(String[] args) {
        messageClass msg = new messageClass();
        msg.message();
    }
}

// compile: (not needed when copiling the package)
// javac userDefinedPackage.java
// compile package:
// javac -d DESTINATION userDefinedPackage.java
// creates the "myPackage"-package
// to run the file (in the compiled package):
// java myPackage.userDefinedPackage