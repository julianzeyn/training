import myPackage2.myPackage2;
// import myPackage2.*; // results in an error due to confusing the class myPackage2.class with the file myPackage2.java

class userDefinedPackage2 {
    public static void main(String[] args) {
        myPackage2 msg = new myPackage2();
        msg.message();
    }
}