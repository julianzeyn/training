/** Some examples of Regular Expressions in Java. */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RegEx {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("hi", Pattern.CASE_INSENSITIVE);
        // Pattern.compile() -> method to create a pattern, the second parameter is optional
        Matcher matcher = pattern.matcher("Hello!");
        Matcher matcher2 = pattern.matcher("Hi!");
        Matcher matcher3 = pattern.matcher("MATCHING!");
        // matcher() -> method to search for the pattern in a string
        // Matcher object contains information about the search
        if (matcher.find()) { // find() -> returns true or false
            System.out.println("Matcher found");
            System.out.println(matcher);
        }
        if (matcher2.find()) {
            System.out.println("Matcher2 found");
            System.out.println(matcher2);
        }
        if (matcher3.find()) {
            System.out.println("Matcher3 found");
            System.out.println(matcher3);
        }
        /*
        Some RegEx patterns:
        Special characters need two backslashes in front of them to find them!
        [abc]         -> character 'a', 'b' or 'c'
        [^abc]        -> a character, that is NOT 'a', 'b' or 'c'
        [0-9]         -> a character in the range from '0' to '9'
        [a-zA-z]      -> any alphabetical
        one|two|three -> "one" or "two" or "three"
        ^one          -> "one" in the beginning
        one$          -> "one" at the end
        .             -> any one character
        (n)           -> groups n
        n+            -> at least one n
        n*            -> zero or more n
        n?            -> zero or one n
        n{x}          -> sequence of x times n
        n{x,y}        -> sequence of at least x but less than y times n
        n{x,}         -> sequence of at least x times n
        (n)\\1        -> backreferences group #1 (find n and save it in memory for later use)
        
        Greedy      Reluctant   Possessive
        n?          n??         n?+
        n*          n*?         n*+
        n+          n+?         n++
        n{}         n{}?        n{}+
        
        Greedy:     starts with entire string and tries to find the regex
                    if not found, try with one character less
        Reluctant:  starts with one character and tries to find the regex
                    if not found, try with one character more
        Possessive: starts with entire string and tries to find the regex
                    if not found, do nothing else
        */
        System.out.println(Pattern.matches(".(et)", "pet")); // true
        System.out.println(Pattern.matches(".(et)", "petroleum")); // false
        System.out.println(Pattern.matches(".(et).*", "petroleum")); // true
        System.out.println(Pattern.matches("[1-9]*", "")); // true
        System.out.println(Pattern.matches("[1-9]", "12345")); // false
        System.out.println(Pattern.matches("[1-9]*", "12345")); // true
        System.out.println(Pattern.matches("[1-9]", "01234")); // false
        
        System.out.println(Pattern.matches("([0-9])\\1{2}", "123")); // false
        System.out.println(Pattern.matches("([0-9])\\1{2}", "333")); // true
    }
}