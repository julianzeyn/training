/** Remind myself which method to use to get the size/length of an object. */

import java.util.ArrayList;

class SizeLength {
    public static void main(String[] args) {
        String string = "A String.";
        System.out.println("String length(): " + string.length());
        // length() -> method, only used for strings
        // length() can change by changing the string
        int[] array = new int[string.length()];
        System.out.println("Array length: " + array.length);
        // length -> not a method (no brackets), used for all arrays
        // length is constant for each array (arrays cannot grow or shrink)
        for (int i = 0; i < string.length(); i++) {
            array[i] = i;
            System.out.println("Array length: " + array.length);
        }        
        ArrayList<Integer> collection = new ArrayList<>();
        System.out.println("Collection size(): " + collection.size());
        // size() -> method, used for all collections
        // size() can change by adding or removing entries from the collection
        for (int i = 0; i < array.length; i++) {
            collection.add(array[i] + i);
            System.out.println("Collection size(): " + collection.size());
        }
    }
}