/** Various methods for the Java Stream API. */

import java.util.stream.*;
import java.util.List;
import java.util.Arrays;
import java.util.IntSummaryStatistics;

class Stream {
    public static void main (String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(numbers);
        int sumSquares = numbers.stream().map(x -> x * x).reduce(0,(ans,i) -> ans + i);
        // map -> returns the result of a function applied to each element
        // reduce -> reduce the elements to a single value
        System.out.println(sumSquares);
        List<String> names = Arrays.asList("Julian", "Ying", "Sabine", "Peter");
        System.out.println(names);
        List<String> sortedNames = names.stream().sorted().collect(Collectors.toList());
        // sorted -> sort the elements
        // collect -> returns a List
        System.out.println(sortedNames);
        sortedNames.stream().filter(s -> s.startsWith("J")).forEach(s -> System.out.println(s));
        // filter -> filter the elements according to some condition
        // forEach -> does something for each of the elements
        
        IntSummaryStatistics stats = numbers.stream().mapToInt((x) -> x).summaryStatistics();
        // gives statistics for integer arrays/lists
        System.out.println(stats.getMax());
        System.out.println(stats.getMin());
        System.out.println(stats.getSum());
        System.out.println(stats.getAverage());
    }
}