// Various methods to work with Strings.
import java.util.Locale;

class Strings {
    public static void main(String[] args) {
        String testString = " This is a String. ";
        System.out.println(testString);
        System.out.println(testString.length());
        System.out.println(testString.charAt(3)); // 0-indexed
        System.out.println(testString.substring(3)); // start at index 3
        System.out.println(testString.substring(3, 8)); // from index 3 to index 8 (exclusive)
        System.out.println(testString.concat(" This as well."));
        System.out.println(testString.indexOf("is")); // index of first occurence
        System.out.println(testString.indexOf("is", 5)); // index of first occurence after index 5
        System.out.println(testString.lastIndexOf("is")); 
        System.out.println(testString.equals(" This is a String. "));
        System.out.println(testString.equalsIgnoreCase(" this is a string. "));
        System.out.println(testString.toLowerCase());
        System.out.println(testString.toUpperCase());
        System.out.println(testString.trim()); // remove spaces in the front and at the end
        System.out.println(testString.replace('i', 'e'));
        
        // String formatting
        System.out.printf("%d %f %n", 42, 0.42);
        System.out.printf("%2$f %2$e %n %1$d %1$o %1$x %n %4$c %3$s %n", 42, 42.1234, "string", 's');
        System.out.printf("|%10d| |%-10d| |% d| |%010d| %n", 1, 2, 3, 4);
        String newString = String.format(Locale.getDefault(), "%,d", 1000000);
        String newString2 = String.format(Locale.US, "%,d", 1000000);
        String newString3 = String.format(Locale.getDefault(), "%.4f", 42.123456);
        String newString4 = String.format(Locale.US, "%.8f", 42.123456);
        System.out.println(newString);
        System.out.println(newString2);
        System.out.println(newString3);
        System.out.println(newString4);
    }
}