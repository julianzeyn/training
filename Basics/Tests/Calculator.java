/** Simple Calculator to test. */

public class Calculator {
    public int evaluate(String expression) {
        int sum = 0;
        for (String summand : expression.split("\\+")) {
            // sum += Integer.valueOf(summand); // test succeeds
            sum = Integer.valueOf(summand); // test fails
        }
        
        return sum;
    }
}