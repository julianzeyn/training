/** Testing the Calculator. */

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest { // has to be public!
  @Test
  public void evaluatesExpression() { // has to be public!
    Calculator calculator = new Calculator();
    int sum = calculator.evaluate("1+2+3");
    assertEquals(6, sum);
  }
}

/*
to compile:
javac -cp ".\junit-4.13.2.jar;.\Basics\Tests\" .\Basics\Tests\CalculatorTest.java
javac -cp "paths to junit.jar and class files (Calculator class in another file)" .\Path\to\file\filename.java

to run:
java -cp ".\junit-4.13.2.jar;.\hamcrest-core-1.3.jar;.\Basics\Tests\" org.junit.runner.JUnitCore CalculatorTest
java -cp "paths to junit.jar and hamcrest.jar and class files" org.junit.runner.JUnitCore (no main method) filename
*/