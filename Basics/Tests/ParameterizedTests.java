/** 
 * Testing with Parameters. 
 * The last test fails to show the output.
 */

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParameterizedTests {
    @Parameters(name = "{index}: addOne({0}) == {1}") // custom name for each test, default: {index}
    public static Collection<Object[]> data() { // static method that returns a Collection of Object arrays
        return Arrays.asList(new Object[][] { {1, 2}, {2, 3}, {3, 4}, {4, 4} }); 
    }
    
    // fields to be filles with the values from the parameters by the constructor
    private int variable;
    private int expected;
    public ParameterizedTests(int variable, int expected) {
        this.variable = variable;
        this.expected = expected;
    }
    // other method (without using a constructor):
    /*
    @Parameter
    public int variable;
    
    @Parameter(1)
    public int expected;
    */
    // IMPORTANT: public, not private
    // @Parameter(0) is the default
    // IMPORTANT: needs import org.junit.runners.Parameterized.Parameter;
    
    @Test
    public void addOneTest() {
        int result = variable + 1;
        assertEquals(expected, result);
    }
}

/*
to compile:
javac -cp ".\junit-4.13.2.jar;.\Basics\Tests\" .\Basics\Tests\ParameterizedTests.java
javac -cp "paths to junit.jar and class files (Calculator class in another file)" .\Path\to\file\filename.java

to run:
java -cp ".\junit-4.13.2.jar;.\hamcrest-core-1.3.jar;.\Basics\Tests\" org.junit.runner.JUnitCore ParameterizedTests
java -cp "paths to junit.jar and hamcrest.jar and class files" org.junit.runner.JUnitCore (no main method) filename
*/