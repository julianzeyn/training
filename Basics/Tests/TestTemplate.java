/** Test method template for JUnit. */

import org.junit.*; // needed for the annotations
import static org.junit.Assert.*; // needed for the assertEquals() method
 
public class TestTemplate {
 
    private java.util.List emptyList;
 
    /**
     * Sets up the test fixture. 
     * (Called before every test case method.)
     */
    @Before
    public void setUp() {
        emptyList = new java.util.ArrayList();
    }
 
    /**
     * Tears down the test fixture. 
     * (Called after every test case method.)
     */
    @After
    public void tearDown() {
        emptyList = null;
    }
    
    @Test
    public void testSomeBehavior() {
        assertEquals("Empty list should have 0 elements", 0, emptyList.size());
    }
 
    @Test(expected=IndexOutOfBoundsException.class)
    public void testForException() {
        Object o = emptyList.get(0);
    }
    
    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main("TestTemplate");
    }
}

/*
to compile:
javac -cp .\junit-4.13.2.jar .\Basics\Tests\TestTemplate.java
javac -cp Path\to\junit\junit-4.13.2.jar Path\to\file\filename.java

to run:
java -cp ".\junit-4.13.2.jar;.\hamcrest-core-1.3.jar;.\Basics\Tests\" TestTemplate
java -cp "Path\to\junit\junit-4.13.2.jar;Path\to\hamcrest\hamcrest-core-1.3.jar;Path\to\file" filename
*/