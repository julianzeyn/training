/** Simple Test method with JUnit. This needs junit-4.13.2.jar */

import org.junit.Test; // needed for the @Test annotation
import static org.junit.Assert.assertEquals; // needed for the assertEquals() method
// IMPORTANT: import as static

class Tests {
    String message = "Teststring";
    @Test
    void testPrintMessage() {
        System.out.println("Inside testPrintMessage()");
        assertEquals(message, "Teststring"); // no output, since it return true
        // assertEquals(message, "Teststring2"); // throws an exception
    }

    public static void main(String[] args) {
        Tests result = new Tests();
        result.testPrintMessage();
    }
}

/* 
to compile:
javac -cp .\junit-4.13.2.jar .\Basics\Tests\Tests.java
javac -cp Path\to\junit\junit-4.13.2.jar Path\to\file\filename.java

to run:
java -cp ".\junit-4.13.2.jar;.\Basics\Tests\" Tests
java -cp "Path\to\junit\junit-4.13.2.jar;Path\to\file" filename
*/