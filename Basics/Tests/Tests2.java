/** Simple Test method with JUnit. This needs junit-4.13.2.jar and hamcrest-core-1.3.jar */

import org.junit.*; // needed for the @Test annotation
import static org.junit.Assert.*; // needed for the assertEquals() method
import java.util.*;
 
public class Tests2 {
    @Test
    public void testEmptyCollection() {
        Collection collection = new ArrayList();
        assertTrue(collection.isEmpty()); // test OK (gives an output)
        assertTrue(!collection.isEmpty()); // test failes
    }
    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main("Tests2");
    }
}

/*
to compile:
javac -cp .\junit-4.13.2.jar .\Basics\Tests\Tests2.java
javac -cp Path\to\junit\junit-4.13.2.jar Path\to\file\filename.java

to run:
java -cp ".\junit-4.13.2.jar;.\hamcrest-core-1.3.jar;.\Basics\Tests\" Tests2
java -cp "Path\to\junit\junit-4.13.2.jar;Path\to\hamcrest\hamcrest-core-1.3.jar;Path\to\file" filename
*/