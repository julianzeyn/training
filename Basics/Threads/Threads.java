/**
 * Threads are for parallel processing.
 * They are implemented by extending the Thread-class or by implementing the Runnable-interface.
 * Comment either the first or the second half of the program to run it!
 */

// When extending the Thread-class, the class cannot extend other classes.
// However, the Thread-class has some inbuilt methods:
// getName, getPrority, isAlive, join (waiting for a thread to terminate), sleep, start


class ThreadTest extends Thread {
    @Override // not necessary, but helps to catch errors during compiling
    public void run() {
        try {
            System.out.println("Thread " + Thread.currentThread().getId() + " is running.");
        } catch (Exception ex) {
            System.out.println("Exception caught!");
        }
    }
}

class Threads {
    public static void main (String[] args) {
        int numberOfThreads = 8;
        for (int i = 0; i < numberOfThreads; i++) {
            ThreadTest testObj = new ThreadTest();  // Syntax to create threads is different!
            testObj.start(); // starts a Thread, which will call the (overridden) run() method
        }
    }
}


// When implementing the Runnable-interface, the class can still extend other classes.
// However, the Runnable-interface (obviously) has no implemented methods.
/*
class ThreadTest implements Runnable {
    @Override // not necessary, but helps to catch errors during compiling
    public void run() {
        try {
            System.out.println("Thread " + Thread.currentThread().getId() + " is running.");
        } catch (Exception ex) {
            System.out.println("Exception caught!");
        }
    }
}

class Threads {
    public static void main (String[] args) {
        int numberOfThreads = 8;
        for (int i = 0; i < numberOfThreads; i++) {
            Thread testObj = new Thread(new ThreadTest()); // Syntax to create threads is different!
            testObj.start(); // starts a Thread, which will call the (overridden) run() method
        }
    }
}
*/