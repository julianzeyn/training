/** Program to show the parallel processing. */

class MyThread implements Runnable {
    String name;
    Thread thread;
    MyThread (String name){
        this.name = name; 
        thread = new Thread(this, name);
        System.out.println("New thread: " + thread);
        thread.start();
    }
    public void run() {
        try {
            for(int i = 5; i > 0; i--) {
            System.out.println(name + ": " + i);
            Thread.sleep(1000);
        }
    } catch (InterruptedException e) {
        System.out.println(name + " interrupted.");
    }
        System.out.println(name + " exiting.");
    }
}
 
class Threads2 {
    public static void main(String[] args) {
        // the threads are working in parallel
        // without threads, each line waits for the previous one to finish
        new MyThread("First");
        new MyThread("Second");
        new MyThread("Third");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Main thread exiting.");
    }
}
 