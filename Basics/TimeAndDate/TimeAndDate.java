/** Short program that returns the current date and time. */

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

class TimeAndDate {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalTime rightnow = LocalTime.now();
        System.out.println("Today's date is: " + today);
        System.out.println("It is " + rightnow.truncatedTo(ChronoUnit.SECONDS) + " right now.");
    }
}