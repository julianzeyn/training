import java.lang.Math;

class math {
	public static void main(String[] args) {
		final double PI = 3.14;
		// final -> constant that cannot be changed
		int x = Math.abs(-10); // sbsolute value
		double y = Math.ceil(PI); // round up
		double z = Math.floor(PI); // round down
		double a = Math.max(y, z); // maximum
		double b = Math.min(y, z); // minimum
		double c = Math.pow(2, 3); // exponential
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}
}