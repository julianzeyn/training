/** TicTacToe-game to play in the console. */
import java.util.Scanner;

class TicTacToe {
    private char[][] input = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
    private boolean gameEnd = false;
    private boolean draw = false;
    private boolean player1 = true;
    private final int ROWS = 11;
    private int position = 0;
    private int counter = 0;
    private String player = "Player1";
    private char mark = 'X';
    private Scanner scanner = new Scanner(System.in);
    
    void printGame() {
        int j = 0;
        for (int i = 0; i < ROWS; i++) {
            if (i == 3 || i == 7) {
                System.out.println("-----------------");
            } else if (i == 1 || i == 5 || i == 9) {
                System.out.println("  " + input[j][0] + "  |  " + input[j][1] + "  |  " + input[j][2] + "  ");
                j++;
            } else {
                System.out.println("     |     |     ");
            }
        }
    }
    
    void getPosition () {
        System.out.println(player + "'s turn!");
        boolean inputType = false;
        while (!inputType) {
            try {
                System.out.print("Please enter a position to place your mark (1 - 9): ");
                position = scanner.nextInt();
                if (position < 1 || position > 9) {
                    System.out.println("Please enter an integer between 1 and 9!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
    }
    
    void placeMark () {
        position--;
        int x = position/3;
        int y = position%3;
        if (input[x][y] == ' ') {
            input[x][y] = mark;
            counter++;
            changePlayer();
        } else {
            System.out.println();
            System.out.println("Field is already occupied!");
            System.out.println("Choose a different field!");
            System.out.println();
            getPosition();
            placeMark();
        }
    }
    
    private void changePlayer() {
        player1 = !player1;
        if (player1) {
            player = "Player1";
            mark = 'X';
        } else {
            player = "Player2";
            mark = 'O';
        }
    }
    
    boolean checkVictory () {
        gameEnd = (gameEnd || checkVictory2(input[0][0], input[1][1], input[2][2]));
        gameEnd = (gameEnd || checkVictory2(input[2][0], input[1][1], input[0][2]));
        for (int i = 0; i < 3; i++) {
            gameEnd = (gameEnd || checkVictory2(input[i][0], input[i][1], input[i][2]));
            gameEnd = (gameEnd || checkVictory2(input[0][i], input[1][i], input[2][i]));
        }
        if (!gameEnd) {
            if (counter == 9) {
                gameEnd = true;
                draw = true;
            }
        }
        if (gameEnd) {
            changePlayer();
            scanner.close();
            System.out.println("Game over!");
            if (draw) {
                System.out.println("It is a draw!");
            } else {
                System.out.println(player + " won!");
                System.out.println("Congratulations!");
            }
        }
        return gameEnd;
    }
    
    private boolean checkVictory2 (char a, char b, char c) {
        if (a == b && a == c && a != ' ') {
            return true;
        }
        return false;
    }
}

class TicTacToeConsole {
    public static void main (String[] args) {
        TicTacToe game = new TicTacToe();
        game.printGame();
        while (!game.checkVictory()) {
            game.getPosition();
            game.placeMark();
            game.printGame();          
        }
    }
}