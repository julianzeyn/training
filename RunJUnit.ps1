﻿param([String]$Path)
$Filepath = Split-Path $Path -Parent
$Filename = Split-Path $Path -Leaf
$File = $Filename.Split(".") | Select-Object -First 1
java -cp ".\junit-4.13.2.jar;.\hamcrest-core-1.3.jar;$Filepath" org.junit.runner.JUnitCore $File