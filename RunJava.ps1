﻿param([String]$Path)
$Filepath = Split-Path $Path -Parent
$Filename = Split-Path $Path -Leaf
$File = $Filename.Split(".") | Select-Object -First 1
java -cp $Filepath $File