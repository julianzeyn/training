/** Returns, whether two input strings are anagrams. */

import java.util.*;
import java.io.*;

class Solution {
    int areAnagram(String S1, String S2) {
        String newS1 = S1.replace(" ", "");
        String newS2 = S2.replace(" ", "");
        char[] string1 = newS1.toLowerCase().toCharArray();
        char[] string2 = newS2.toLowerCase().toCharArray();
        if (string1.length == string2.length) {
            Arrays.sort(string1);
            Arrays.sort(string2);
            for (int i = 0; i < string1.length; i++) {
                if (string1[i] != string2[i]) {
                    return 0;
                }
            }
            return 1;
        }
        return 0;
    }
}

class AnagramCheck {
    public static void main(String args[]) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = {"", ""};
        String[] inputRequest = new String[2];
        inputRequest[0] = "Please enter a text: ";
        inputRequest[1] = "Please enter another text: ";
        boolean[] correctInput = {false, false};
        for (int i = 0; i < input.length; i++) {
            while (!correctInput[i]) {
                System.out.print(inputRequest[i]);
                input[i] = reader.readLine();
                if (!(input[i].matches("^[A-Za-z ]+$"))) {
                    System.out.println("Text must not contain numbers or symbols!");
                } else {
                    correctInput[i] = true;
                }
            }
        }
        Solution ob = new Solution();
        switch (ob.areAnagram(input[0], input[1])) {
            case 0:
                System.out.println("\'" + input[0] + "\' and \'" + input[1] + "\' are not anagrams.");
                break;
            case 1:
                System.out.println("\'" + input[0] + "\' and \'" + input[1] + "\' are anagrams!");
                break;
        }
    }
}


