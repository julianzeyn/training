/** Testing the AnagramCheck program. */

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AnagramCheckTest {
    @Test
    public void areAnagramTest() {
        Solution testObj = new Solution();
        int result = testObj.areAnagram("abc", "cba");
        int expectedResult = 1;
        assertEquals(expectedResult, result);
    }
    @Test
    public void spacesTest() {
        Solution testObj = new Solution();
        int result = testObj.areAnagram("abc    ", "  a  c  b  ");
        int expectedResult = 1;
        assertEquals(expectedResult, result);
    }
    @Test
    public void capitalizeTest() {
        Solution testObj = new Solution();
        int result = testObj.areAnagram("abc", "ABC");
        int expectedResult = 1;
        assertEquals(expectedResult, result);
    }
    @Test
    public void notAnagramTest() {
        Solution testObj = new Solution();
        int result = testObj.areAnagram("abc", "aabc");
        int expectedResult = 0;
        assertEquals(expectedResult, result);
    }
}