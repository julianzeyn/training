/** Short dice game against the computer. You win when your number is greater! */

import java.util.Random;

class DiceGame {
    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception ex) {
            System.out.println("Error during waiting time!");
        }
    }
    
    public static void main(String[] args) {
        boolean win = false;
        int counter = 0;
        Random random = new Random();
        System.out.println("Lets\'s play a dice game!");
        wait(1000);
        System.out.println("You win when your number is greater than mine!");
        wait(1000);
        while (!win) {
            int myNumber = random.nextInt(6) + 1;
            int yourNumber = random.nextInt(6) + 1;
            System.out.println("Your number is: " + yourNumber);
            wait(1000);
            System.out.println("My number is: " + myNumber);
            wait(1000);
            if (yourNumber > myNumber) {
                System.out.println("You won!");
                wait(1000);
                if (counter == 1) {
                    System.out.println("I won " + counter + " round, before you won 1 round!");
                } else {
                    System.out.println("I won " + counter + " rounds, before you won 1 round!");
                }
                win = true;
            } else {
                counter++;
                System.out.println("I won!");
                wait(1000);
                System.out.println("Let\'s play another round!");
                System.out.println("");
                wait(1000);
            }
        }
    }
}