/** Prints the factorial of an input number. */

import java.util.Scanner;

class Factorial {
    static int calculateFactorial (int input) {
        int result = 1;
        for (int i = input; i > 0; i--) {
            result *= i;
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean inputType = false;
        int input = 0;
        while (!inputType) {
            try {
                System.out.print("Please enter an integer: ");
                input = scanner.nextInt();
                if (input < 1) {
                    System.out.println("Please enter an integer between 1 and 12!");
                } else if (input > 12) {
                    System.out.println("Please enter an integer between 1 and 12!");
                    System.out.println("'" + input + "!' is larger than 'Integer.MAX_VALUE'!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
        System.out.println("The factorial of " + input + " is: " + calculateFactorial(input));
    }
}