/** Testing the Factorial.java program*/

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class FactorialTest {
    @Parameters(name = "{index}: calculateFactorial({0}) == {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { {1, 1}, {2, 2}, {3, 6}, {4, 24}, {5, 120} }); 
    }
    
    @Parameter
    public int input;
    @Parameter(1)
    public int expectedResult;
    
    @Test
    public void calculateFactorialTest () {
        Factorial testObj = new Factorial();
        assertEquals(expectedResult, testObj.calculateFactorial(input));
    }
}