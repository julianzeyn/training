/** 
 * Returns all factors of an entered integer. 
 * Returns an error message when the input is not an integer and asks for an integer again. 
 */

import java.util.Scanner;

class Factors {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int input = 0;
        boolean inputType = false;
        while (!inputType) {
            System.out.println("Please enter an integer:");
            try { 
                input = scan.nextInt();
                inputType = true;
            } catch (Exception e) {
                System.out.println("Error: Input is not an integer!");
                scan.next();
            }
        }
        scan.close();
        String output = input + " can be divided by: ";
        for (int i = 1; i < input; i++) {
            if (input % i == 0) {
                output += i + ", ";
            }
        }
        output += input;
        System.out.println(output);
    }
}