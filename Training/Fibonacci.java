/** Returns the first n values of the Fibonacci sequence. */

import java.util.Scanner;
import java.util.ArrayList;

class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean inputType = false;
        int input = 0;
        while (!inputType) {
            try {
                System.out.print("Please enter an integer: ");
                input = scanner.nextInt();
                if (input < 1) {
                    System.out.println("Please enter an integer greater than 0!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
        ArrayList<Integer> result = new ArrayList<>();
        if (input == 1) {
            result.add(0);
        } else {
            result.add(0);
            result.add(1);
        }
        for (int i = 2; i < input; i++) {
            result.add(result.get(i - 2) + result.get(i - 1));
        }
        System.out.println("The first " + input + " values of the Fibonacci sequence are: ");
        System.out.println(result);
    }
}