/** Simple calculator with a GUI, but only for integers.*/

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

class GuiIntegerCalculator {
    public static void main(String[] args) {
        GuiCalculator calc = new GuiCalculator();
    }
}

class GuiCalculator extends JFrame {
    private static final int HEIGHT = 400;
    private static final int WIDTH = 700;
    
    private JLabel firstLabel;
    private JLabel secondLabel;
    private JLabel resultLabel;
    
    private JTextField firstText;
    private JTextField secondText;
    private JTextField resultText;
    
    private JButton addButton;
    private JButton subButton;
    private JButton multButton;
    private JButton divButton;
    private JButton clearButton;
    private JButton exitButton;
    
    private AddButtonHandler addButtonHandler;
    private SubButtonHandler subButtonHandler;
    private MultButtonHandler multButtonHandler;
    private DivButtonHandler divButtonHandler;
    private ClearButtonHandler clearButtonHandler;
    private ExitButtonHandler exitButtonHandler;
    
    GuiCalculator () {
        firstLabel = new JLabel("Enter the first integer: ");
        secondLabel = new JLabel("Enter the second integer: ");
        resultLabel = new JLabel("The integer result is: ");
        
        firstText = new JTextField();
        secondText = new JTextField();
        resultText = new JTextField();
        
        addButton = new JButton("+");
        addButtonHandler = new AddButtonHandler();
        addButton.addActionListener(addButtonHandler);
        subButton = new JButton("-");
        subButtonHandler = new SubButtonHandler();
        subButton.addActionListener(subButtonHandler);
        multButton = new JButton("*");
        multButtonHandler = new MultButtonHandler();
        multButton.addActionListener(multButtonHandler);
        divButton = new JButton("/");
        divButtonHandler = new DivButtonHandler();
        divButton.addActionListener(divButtonHandler);
        clearButton = new JButton("CE");
        clearButtonHandler = new ClearButtonHandler();
        clearButton.addActionListener(clearButtonHandler);
        exitButton = new JButton("Close");
        exitButtonHandler = new ExitButtonHandler();
        exitButton.addActionListener(exitButtonHandler);
        
        setTitle("Integer Calculator");
        Container pane = getContentPane();
        pane.setLayout(new GridLayout (6, 2));
        pane.add(firstLabel);
        pane.add(firstText);
        pane.add(secondLabel);
        pane.add(secondText);
        pane.add(resultLabel);
        pane.add(resultText);
        pane.add(addButton);
        pane.add(subButton);
        pane.add(multButton);
        pane.add(divButton);
        pane.add(clearButton);
        pane.add(exitButton);
        
        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE); 
    }
    
    private class AddButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int a = 0;
            int b = 0;
            try {
                a = Integer.parseInt(firstText.getText());
                b = Integer.parseInt(secondText.getText());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Not an Integer!", "Integer warning", JOptionPane.ERROR_MESSAGE);
            }
            int result = a + b;
            resultText.setText("" + result);
        }
    }
    
    private class SubButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int a = 0;
            int b = 0;
            try {
                a = Integer.parseInt(firstText.getText());
                b = Integer.parseInt(secondText.getText());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Not an Integer!", "Integer warning", JOptionPane.ERROR_MESSAGE);
            }
            int result = a - b;
            resultText.setText("" + result);
        }
    }
    
    private class MultButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int a = 0;
            int b = 0;
            try {
                a = Integer.parseInt(firstText.getText());
                b = Integer.parseInt(secondText.getText());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Not an Integer!", "Integer warning", JOptionPane.ERROR_MESSAGE);
            }
            int result = a * b;
            resultText.setText("" + result);
        }
    }
    
    private class DivButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int a = 0;
            int b = 1;
            int result = 0;
            try {
                a = Integer.parseInt(firstText.getText());
                b = Integer.parseInt(secondText.getText());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Not an Integer!", "Integer warning", JOptionPane.ERROR_MESSAGE);
            }
            try {
            result = a / b;
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Division by 0! \nPlease change the second integer value!", "Error!", JOptionPane.ERROR_MESSAGE);
            }
            resultText.setText("" + result);
        }
    }
    
    private class ClearButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            firstText.setText("");
            secondText.setText("");
            resultText.setText("");
        }
    }
    
    private class ExitButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}