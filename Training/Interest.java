/** Returns the balance after x years of interest. */

import java.util.Scanner;
import java.util.Locale;

class Interest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in).useLocale(Locale.US);
        double[] input = {0., 0., 0.};
        String[] inputRequest = new String[3];
        inputRequest[0] = "Balance: ";
        inputRequest[1] = "Interest rate p.a. (0.05 = 5%): ";
        inputRequest[2] = "Number of years: ";
        boolean[] correctInput = {false, false, false};
        for (int i = 0; i < input.length; i++) {
            while (!correctInput[i]) {
                System.out.println(inputRequest[i]);
                try {
                    input[i] = scan.nextDouble();
                    if (input[i] < 0) {
                        System.out.println("Please enter a number > 0!");
                        
                    } else {
                        correctInput[i] = true;
                    }
                } catch (Exception e) {
                    System.out.println("Please enter a number > 0!");
                    scan.next();
                }
            }
        }
        
        for (int i = 1; i < input[2] + 1; i++) {
            input[0] *= (input[1] + 1.) ;
            if (i == 1) {
                System.out.printf(Locale.US, "Balance after 1 year: %.2f Euro\n", input[0]);
            } else {
                System.out.printf(Locale.US, "Balance after " + i + " years: %.2f Euro\n", input[0]);
            }
        }            
        scan.close();        
    }
}