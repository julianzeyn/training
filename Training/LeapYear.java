/**
 * Test whether a given year (int) is a leap year.
 * Use a method that returns a boolean.
 */

import java.util.Scanner;

class LeapYear {
    static boolean isLeapYear(int year) {
        if (year % 400 == 0) {
            return true;
        } else if ((year % 4 == 0) && !(year % 100 == 0)) {
            return true;     
        } else {
            return false;
        }
        // shorter possibility: return ((year % 400 == 0) || ((year % 4 == 0) && !(year % 100 == 0)));
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int year = 0;
        boolean inputType = false;
        while (!inputType) {
            System.out.println("Please enter a year (integer):");
            try { 
                year = scan.nextInt();
                inputType = true;
            } catch (Exception e) {
                System.out.println("Error: Input is not an integer!");
                scan.next();
            }
        }
        scan.close();
        
        if (isLeapYear(year)) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is not a leap year.");
        }
    }
}