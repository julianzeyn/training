/** Testing the LeapYear program. */

import java.util.Arrays;
import java.util.Collection;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class LeapYearTest {
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { {2000, true}, {2004, true}, {2008, true}, {2040, true}, {2400, true}, {1900, false}, {2001, false}, {1994, false} });
    }
    
    private int year;
    private boolean expected;
    public LeapYearTest(int year, boolean expected) {
        this.year = year;
        this.expected = expected;
    }
    
    @Test
    public void isLeapYearTest() {
        assertEquals(expected, LeapYear.isLeapYear(year));
    }
}