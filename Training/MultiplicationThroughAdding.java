/** Returns the product of two input-integers using only addition. */

import java.util.Scanner;

class MultiplicationThroughAdding {
    static int input (Scanner scanner) {
        boolean inputType = false;
        int input = 0;
        while (!inputType) {
            try {
                System.out.print("Please enter an integer: ");
                input = scanner.nextInt();
                if (input < 0) {
                System.out.println("Please enter a positive integer!");
                } else {
                inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
        return input;
    }
    
    static int multiplication (int a, int b) {
        int result = 0;
        for (int i = 0; i < a; i++) {
            result += b;
        }
        return result;
    }
    
    public static void main (String[] args) {
        int a = 0;
        int b = 0;
        Scanner scanner = new Scanner(System.in);
        a = input(scanner);
        b = input(scanner);
        int result = 0;
        if (a < b) {
            result = multiplication(a, b);
        } else {
            result = multiplication(b, a);
        }
        System.out.println(a + " * " + b + " = " + result);
        scanner.close();
    }
}