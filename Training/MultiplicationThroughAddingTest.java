/** Testing the MultiplicationThroughAdding.java program*/

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class MultiplicationThroughAddingTest {
    @Parameters(name = "{index}: calculateFactorial({0}) == {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { {1, 1}, {2, 2}, {3, 6}, {4, 5}, {5, 5} }); 
    }
    
    @Parameter
    public int a;
    @Parameter(1)
    public int b;
    
    @Test
    public void multiplicationTest () {
        MultiplicationThroughAdding testObj = new MultiplicationThroughAdding();
        int expectedResult = a * b;
        assertEquals(expectedResult, testObj.multiplication(a, b));
    }
}