/** Returns an input decimal integer as a binary, octal and hexadecimal number using two different methods. */

import java.util.Scanner;

class NumberConverter {
    static void binary(int n) {
        if (n > 1) {
            binary(n / 2);
        }
        System.out.print(n % 2);
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean inputType = false;
        int input = 0;
        while (!inputType) {
            try {
                System.out.print("Please enter a positive integer: ");
                input = scanner.nextInt();
                if (input < 1) {
                    System.out.println("Please enter an integer greater than 0!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
        System.out.printf("Decimal:     %d%n", input);
        System.out.printf("Binary:      ");
        binary(input);
        System.out.printf("%n");
        System.out.printf("Octal:       %o%n", input);
        System.out.printf("Hexadecimal: %x%n", input);
        System.out.println("");
        System.out.println("2nd method:");
        System.out.println("Decimal:     " + input);
        System.out.println("Binary:      " + Integer.toString(input,2));
        System.out.println("Octal:       " + Integer.toString(input,8));
        System.out.println("Hexadecimal: " + Integer.toString(input,16));
    }
}