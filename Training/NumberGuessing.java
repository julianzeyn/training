/** Game to guess the random number between 0 and 100 of the computer. */

import java.util.Random;
import java.util.Scanner;

class NumberGuessing {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        final int number = random.nextInt(101);
        boolean found = false;
        int guess = 0;
        int counter = 0;
        System.out.println("I chose a number between 0 and 100.");
        System.out.println("Guess my number!");
        while (!found) {
            System.out.print("Please enter an integer between 0 and 100: ");
            try {
                guess = scanner.nextInt();
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
                continue;
            }
            counter++;
            if (guess == number) {
                System.out.println("You found it!");
                System.out.println("My number was " + number + "!");
                if (counter ==1) {
                    System.out.println("You needed only " + counter + " guess!");
                } else {
                    System.out.println("You needed " + counter + " guesses!");
                }
                found = true;
            } else if (guess < number) {
                System.out.println("My number is larger!");
            } else {
                System.out.println("My number is smaller!");
            }
        }
        scanner.close();
    }
}