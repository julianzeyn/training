/** Returns whether n is a perfect number. */

import java.util.Scanner;
import java.util.ArrayList;

class PerfectNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean inputType = false;
        int input = 0;
        while (!inputType) {
            try {
                System.out.print("Please enter a positive integer: ");
                input = scanner.nextInt();
                if (input < 1) {
                    System.out.println("Please enter an integer greater than 0!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Not an integer!");
                scanner.next();
            }
        }
        ArrayList<Integer> divisors = new ArrayList<>();
        divisors.add(1);
        for (int i = 2; i <= input/2; i++) {
            if ((input % i) == 0) {
                divisors.add(i);
            }
        }
        int sum = 0;
        for (int i = 0; i < divisors.size(); i++) {
            sum += divisors.get(i);
        }
        if (sum == input && input != 1) {
            System.out.println(input + " is a perfect number!");
            System.out.print(divisors.get(0));
            for (int i = 1; i < divisors.size(); i++) {
                System.out.print(" + " + divisors.get(i));
            }
            System.out.print(" = " + input + "\n");
        } else {
            System.out.println(input + " is not a perfect number!");
        }
    }
}