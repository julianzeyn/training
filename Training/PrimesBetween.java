/** Calculates and returns all primes between two given integers. */

import java.util.Scanner;
import java.util.ArrayList;

class PrimesBetween {
    static void allPrimes(int a, int b, ArrayList<Integer> primes) {
        for (int i = a; i <= b ; i++) {
            boolean prime = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    prime = false;
                }
            }
            if (prime) {
                primes.add(i);
            }
        }
    }
        
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] input = {0, 0};
        String[] inputRequest = new String[2];
        inputRequest[0] = "Please enter an integer: ";
        inputRequest[1] = "Please enter another integer: ";
        boolean[] correctInput = {false, false};
        for (int i = 0; i < input.length; i++) {
            while (!correctInput[i]) {
                System.out.print(inputRequest[i]);
                try {
                    input[i] = scan.nextInt();
                    if (input[i] < 2) {
                        System.out.println("Please enter a number > 1!");
                    } else {
                        correctInput[i] = true;
                    }
                } catch (Exception e) {
                    System.out.println("Please enter an integer!");
                    scan.next();
                }
            }
        }
        ArrayList<Integer> primes = new ArrayList<>();
        if (input[0] < input[1]) {
            allPrimes(input[0], input[1], primes);
            System.out.println("The primes between " + input[0] + " and " + input[1] + " are:");
        } else {
            allPrimes(input[1], input[0], primes);
            System.out.println("The primes between " + input[1] + " and " + input[0] + " are:");
        }
        System.out.println(primes);
    }
}