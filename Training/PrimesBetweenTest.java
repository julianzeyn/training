/** Testing the PrimesBetween.java program*/

import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class PrimesBetweenTest {
    @Parameters(name = "{index}: PrimesBetween({0}, {1})")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { {2, 9}, {8, 15}, {75, 6}, {34, 34} }); 
    }
    
    private int a;
    private int b;
    private ArrayList<Integer> expectedPrimes;
    private final int[] primesBelow100 = {2, 3, 5, 7, 11, 13, 17, 19, 23,29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
    
    public PrimesBetweenTest(int a, int b) {
        this.a = a;
        this.b = b;
        expectedPrimes = new ArrayList<>();
        for (int i = 0; i < primesBelow100.length; i++) {
            if (a <= primesBelow100[i] && b >= primesBelow100[i]) {
                expectedPrimes.add(primesBelow100[i]);
            }
            if (primesBelow100[i] > b) {
                break;
            }
        }      
    }
    
    @Test
    public void allPrimesTest() {
        PrimesBetween testObj = new PrimesBetween();
        ArrayList<Integer> primes = new ArrayList<>();
        testObj.allPrimes(a, b, primes);
        assertEquals(expectedPrimes, primes);
    }
}