/** Returns the sum of digits of a given integer between 1 and 99'999. */

import java.util.Scanner;
import java.lang.Math;
import java.util.Iterator;
import java.util.ArrayList;

class SumOfDigits {
    public static void main(String[] args) {
        // get the input from the user
        Scanner scan = new Scanner(System.in);
        int input = 0;
        boolean inputType = false;
        while (!inputType) {
            System.out.println("Please enter an integer between 1 and 99'999:");
            try { 
                input = scan.nextInt();
                if (input > 0 && input < 100000) {
                    inputType = true;
                } else {
                    System.out.println("Please enter an integer between 1 and 99'999!");
                }
            } catch (Exception e) {
                System.out.println("Error: Input is not an integer!");
                scan.next();
            }
        }
        scan.close();
        
        // get the digits of the integer
        ArrayList<Integer> digits = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            digits.add(input / (int) Math.pow(10, 4-i));
            input %= (int) Math.pow(10, 4-i);
        }
        
        // get the sum of digits
        int sum = 0;
        for (int i = 0; i < digits.size(); i++) {
            sum += digits.get(i);
        }
        
        // delete the leading 0s
        Iterator<Integer> iter = digits.iterator();
        boolean bool = true;
        while (bool) {
            if(iter.next() == 0) {
                iter.remove();
            } else {
                bool = false;
            }
        }
        
        // create the string
        String output = "The sum of digits is: ";
        for (int i = 0; i < digits.size(); i++) {
            if (i == digits.size() - 1) {
                output += digits.get(i);
            } else {
                output += digits.get(i) + " + ";
            }
        }
        System.out.println(output + " = " + sum);
    }
}