/**
 * Return the western and (approximate) chinese Zodiac for an input birthdate.
 * Additionally returns the next occurence of the chinese Zodiac.
 */

import java.util.Scanner;
import java.util.Calendar;

class Zodiac {
    public static void main (String[] args) {
        Calendar today = Calendar.getInstance();
        int[] todaysDate = {today.get(5), today.get(2) + 1, today.get(1)};
        System.out.println("Today's date is: " + todaysDate[0] + "." + todaysDate[1] + "." + todaysDate[2]);
        System.out.println("Today's zodiac is:");
        WesternZodiac todaysWesternZodiac = new WesternZodiac(todaysDate);
        todaysWesternZodiac.getZodiac();
        ChineseZodiac todaysChineseZodiac = new ChineseZodiac(todaysDate);
        todaysChineseZodiac.getZodiac();
        
        System.out.println();
        
        Scanner scanner = new Scanner(System.in);
        Input input = new Input(scanner);
        int[] date = input.getDate();
        System.out.println("Your birthdate is: " + date[0] + "." + date[1] + "." + date[2]);
        System.out.println("Your zodiac is:");
        WesternZodiac westernZodiac = new WesternZodiac(date);
        westernZodiac.getZodiac();
        ChineseZodiac chineseZodiac = new ChineseZodiac(date);
        chineseZodiac.getZodiac();
        chineseZodiac.nextOccurence(todaysDate);
    }
}

class ChineseZodiac {
    private final String[] CHINESE_ZODIACS = {"Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"};
    private final String[] ELEMENTS = {"Metal", "Water", "Wood", "Fire", "Earth"};
    private int year;
    private int month;
    private int day;
    
    ChineseZodiac (int[] date) {
        day = date[0];
        month = date[1];
        year = date[2];
    }
    
    void getZodiac() {
        if (month == 1 && day < 21) {
            System.out.println("Chinese zodiac: " + ELEMENTS[((year - 1 - 1900)%10)/2] + "-" + CHINESE_ZODIACS[(year - 1 - 1900)%12]);
        } else if (month > 2 || (month == 2 && day > 20)) {
            System.out.println("Chinese zodiac: " + ELEMENTS[((year - 1900)%10)/2] + "-" + CHINESE_ZODIACS[(year - 1900)%12]);
        } else {
            System.out.println("Chinese zodiac (one of these two): " + ELEMENTS[((year - 1 - 1900)%10)/2] + "-" + CHINESE_ZODIACS[(year - 1 - 1900)%12] + ", " + ELEMENTS[((year - 1900)%10)/2] + "-" + CHINESE_ZODIACS[(year - 1900)%12]);
        }
    }
    
    void nextOccurence(int[] todaysDate) {
        int year12 = year + 12;
        int year60 = year + 60;
        while (year12 < todaysDate[2]) {
            year12 += 12;
        }
        while (year60 < todaysDate[2]) {
            year60 += 60;
        }
        if (month == 1 && day < 21) {
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1 - 1900)%10)/2 + 1] + "-)" + CHINESE_ZODIACS[(year - 1 - 1900)%12] + " is in the year: " + year12);
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1 - 1900)%10)/2] + "-)" + CHINESE_ZODIACS[(year - 1 - 1900)%12] + " is in the year: " + year60);
        } else if (month > 2 || (month == 2 && day > 20)) {
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1900)%10)/2 + 1] + "-)" + CHINESE_ZODIACS[(year - 1900)%12] + " is in the year: " + year12);
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1900)%10)/2] + "-)" + CHINESE_ZODIACS[(year - 1900)%12] + " is in the year: " + year60);
        } else {
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1 - 1900)%10)/2 + 1] + "-)" + CHINESE_ZODIACS[(year - 1 - 1900)%12] + " or (" + ELEMENTS[((year - 1900)%10)/2 + 1] + "-)" + CHINESE_ZODIACS[(year - 1900)%12] + " is in the year: " + year12);
            System.out.println("Your next birthday in the year of the (" + ELEMENTS[((year - 1 - 1900)%10)/2] + "-)" + CHINESE_ZODIACS[(year - 1 - 1900)%12] + " or (" + ELEMENTS[((year - 1900)%10)/2] + "-)" + CHINESE_ZODIACS[(year - 1900)%12] + " is in the year: " + year60);
        }
    }
}

class WesternZodiac {
    private int date;
    private final int AQUARIUS = 120;
    private final int PISCES = 219;
    private final int ARIES = 321;
    private final int TAURUS = 420;
    private final int GEMINI = 521;
    private final int CANCER = 621;
    private final int LEO = 723;
    private final int VIRGO = 823;
    private final int LIBRA = 923;
    private final int SCORPIO = 1023;
    private final int SAGITTARIUS = 1123;
    private final int CAPRICORN = 1222;
    
    WesternZodiac (int[] date) {
        this.date = date[1] * 100 + date[0];
    }
    
    void getZodiac() {
        String zodiac = "";
        if (date < AQUARIUS || date > CAPRICORN) {
            zodiac = "Capricorn";
        } else if (date < PISCES) {
            zodiac = "Aquarius";
        } else if (date < ARIES) {
            zodiac = "Pisces";
        } else if (date < TAURUS) {
            zodiac = "Aries";
        } else if (date < GEMINI) {
            zodiac = "Taurus";
        } else if (date < CANCER) {
            zodiac = "Gemini";
        } else if (date < LEO) {
            zodiac = "Cancer";
        } else if (date < VIRGO) {
            zodiac = "Leo";
        } else if (date < LIBRA) {
            zodiac = "Virgo";
        } else if (date < SCORPIO) {
            zodiac = "Libra";
        } else if (date < SAGITTARIUS) {
            zodiac = "Scorpio";
        } else if (date < CAPRICORN) {
            zodiac = "Sagittarius";
        }
        System.out.println("Western zodiac: " + zodiac);
    }
}

class Input {
    private Scanner scanner;
    private int year;
    private int month;
    private int day;
    private boolean leapyear;
    
    Input(Scanner scanner) {
        this.scanner = scanner;
    }
    
    int[] getDate() {
        getYear();
        isLeapYear();
        getMonth();
        getDay();
        int[] date = {day, month, year};
        return date;
    }
    
    private void getYear() {
        boolean inputType = false;
        while(!inputType) {
            System.out.print("Please enter your birthyear: ");
            try {
                year = scanner.nextInt();
                if (year < 1900 || year > 2100) {
                    System.out.println("Please enter a year between 1900 and 2100.");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Please enter an integer!");
                scanner.next();
            }
        }
    }
    
    private void getMonth() {
        boolean inputType = false;
        while(!inputType) {
            System.out.print("Please enter your birthmonth: ");
            try {
                month = scanner.nextInt();
                if (month < 1 || month > 12) {
                    System.out.println("Please enter a month between 1 and 12.");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Please enter an integer!");
                scanner.next();
            }
        }
    }
    
    private void getDay() {
        boolean inputType = false;
        while(!inputType) {
            System.out.print("Please enter your birthday: ");
            try {
                day = scanner.nextInt();
                if (day < 1 || day > 31) {
                    System.out.println("Please enter a day between 1 and 31.");
                } else if (day > 29 && month == 2) {
                    System.out.println(day + ".2. is not a valid date!");
                    if (leapyear) {
                        System.out.println("This month has only 29 days!");
                    } else {
                        System.out.println("This month has only 28 days!");
                    }
                } else if (day == 29 && month == 2 && !leapyear) {
                    System.out.println("29.2." + year + " is not a valid date!");
                    System.out.println(year + " is not a leapyear!");
                } else if (day == 31 && (month == 4 || month == 6 || month == 9 || month == 11)) {
                    System.out.println(day + "." + month + ". is not a valid date!");
                    System.out.println("This month has only 30 days!");
                } else {
                    inputType = true;
                }
            } catch (Exception ex) {
                System.out.println("Please enter an integer!");
                scanner.next();
            }
        }
    }
    
    private void isLeapYear() {
        leapyear = ((year % 400 == 0) || ((year % 4 == 0) && !(year % 100 == 0)));
    }
}